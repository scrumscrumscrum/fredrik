package marstravel;

/**
 *
 * @author Scrum Group 2
 */
public class MarsTravel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String test = "Fredrik Bogren";
        char[] chars = test.toCharArray();
        for (char c : chars) {
            if (Character.isLetter(c) || Character.isSpaceChar(c)) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }
    }

}
