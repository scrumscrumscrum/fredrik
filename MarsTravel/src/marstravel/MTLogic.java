package marstravel;

import javax.swing.JOptionPane;

/**
 *
 * @author leho9818
 * @version 2017-02-17
 */
public class MTLogic {

    RegistreraKundGUI gui = new RegistreraKundGUI();

    /**
     * Code for verification button in GUI
     */
    public void CreatePerson(String inFirstName, String inLastName, String inPNumber, String inAddres, String inCountry, String inTown, String inZipCode) {
        String tmpFirstName = NameLogic(inFirstName);
        String tmpLastName = NameLogic(inLastName);

        String tmpPNumber = PersonalNumberLogic(inPNumber);
        String tmpZipCode = NumberLogic(inZipCode);

        String lolName = "";
        if (tmpFirstName == "false" || tmpLastName == "false") {
            lolName = "Namnet är felaktigt\n";
        }
        if (tmpPNumber == "false") {
            lolName += "Peronsnummeret är fel\n";
        }
        if (inAddres.isEmpty()) {
            lolName += "Adressen är felaktig";
        }
        if (tmpZipCode == "false") {
            lolName += "Postkoden är felaktig";
        }
        if (lolName != "") {
            JOptionPane.showMessageDialog(gui, lolName);
        }
        if (lolName == "") {
            int choice = JOptionPane.showConfirmDialog(gui, "Namn: " + inFirstName + " " + inLastName
                    + "\nPersonnummer: " + inPNumber
                    + "\nAdress: " + inCountry + " " + inTown + " " + inZipCode + " " + inAddres
                    + "\nVill du ändra något?", "Bekräfta", JOptionPane.YES_NO_OPTION);
            if (choice == 1) {
                MTPerson person = new MTPerson();
            }
        }
    }

    /**
     * Code for the Name button in GUI
     *
     * @return name of the customer as String
     */
    public String NameLogic(String inName) {
        char[] chars = inName.toCharArray();
        for (char c : chars) {
            if (!Character.isLetter(c) && !Character.isSpaceChar(c)) {
                //System.out.println("true");
                inName = "false";
                break;
            }
        }
        System.out.println(inName);
        return inName;

    }

    /**
     * Code for confirming that the personal code number is valid
     *
     * @return
     */
    public String PersonalNumberLogic(String inPnumber) {
        char[] chars = inPnumber.toCharArray();
        for (char c : chars) {
            if (!Character.isDigit(c) && c != '-') {
                inPnumber = "false";
                break;
            }
        }
        System.out.println(inPnumber);
        return inPnumber;
    }

    /**
     * Code for confirming that the address is valid
     *
     * @return
     */
    public String AddressLogic() {
        return "s";
    }

    public String NumberLogic(String inNumber) {
        char[] chars = inNumber.toCharArray();
        for (char c : chars) {
            if (!Character.isDigit(c)) {
                inNumber = "false";
                break;

            }
        }
        return inNumber;
    }
}
