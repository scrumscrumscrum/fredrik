/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravel;

/**
 *
 * @author erika
 */
public enum MTFood {
    Lyx1("Lyxmat", 200000);

    private final String desc;
    private final int price;

    MTFood(String description, int cost) {
        desc = description;
        price = cost;

    }

    public String getDescription() {
        return desc;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return getDescription();
    }

    public String[] toStringArray() {
        int count = 0;
        for (MTFood asd : MTFood.values()) {
            count++;
        }
        String[] test = new String[count];
        int i = 0;
        for (MTFood MTF : MTFood.values()) {
            test[i] = MTF.getDescription();
            i++;
        }

        return test;
    }
}
