/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravel;

/**
 *
 * @author erika
 */
    public enum MTLogi {
    SVIT ("Svit", 1200000),
    SPACESIDE ("Spaceside", 700000),
    INSIDE ("Inside", 300000),
    ECONOMY ("Economy", 180000);
    
    
    
    private final String desc;
    private final int price;
    
    
    MTLogi(String description, int cost){
        desc = description;
        price = cost;
        
    }
    public String getDescription(){
        return desc;
    }
    public int getPrice(){
        return price;
    }

}
